# example-percy-jekyll [![This project is using Percy.io for visual regression testing.](https://percy.io/static/images/percy-badge.svg)](https://percy.io/percy/example-percy-jeykll) [![CircleCI](https://circleci.com/gh/percy/example-percy-jekyll.svg?style=svg)](https://circleci.com/gh/percy/example-percy-jekyll)

Example app demonstrating Percy with a Jekyll created site.

Thank you to [CloudCannon](https://cloudcannon.com) for the [Hydra](https://github.com/CloudCannon/hydra-jekyll-template) Jeykll theme!
